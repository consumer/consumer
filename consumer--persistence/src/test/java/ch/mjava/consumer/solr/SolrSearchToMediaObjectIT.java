package ch.mjava.consumer.solr;

import ch.mjava.consumer.persistence.beans.Media;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Testing the solr result conversion, tomcat with solr must
 * be running on 8080 with webapp path solr and contain some data
 *
 * @author knm
 */
public class SolrSearchToMediaObjectIT
{
    private SolrServer server;

    @Before
    public void setUp() throws IOException, SolrServerException
    {
        String url = "http://localhost:8080/solr";
        server = new CommonsHttpSolrServer(url);
    }

    @Test
    public void testSolrResultConversion() throws Exception
    {
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        QueryResponse response = server.query(query);
        int resultSize = response.getResults().size();
        assertTrue("should have gotten some results", resultSize > 0);

        List<Media> medias = response.getBeans(Media.class);
        assertTrue("should have converted some results!", medias.size() > 0);


        //enable this to delete all from the solr server
//        server.deleteByQuery("*:*");
//        server.commit();
    }
}
