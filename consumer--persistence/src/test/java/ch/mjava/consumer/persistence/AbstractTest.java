package ch.mjava.consumer.persistence;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import java.util.ArrayList;
import java.util.Map;

/**
 * Testing Parent
 *
 * @author knm
 */
public abstract class AbstractTest extends
        AbstractTransactionalDataSourceSpringContextTests
{
    @Override
    protected String[] getConfigLocations()
    {
        return new String[]
                { "classpath:persistence-test.xml" };
    }

    protected <T> T getBean(String name, Class<T> requiredType)
    {
        return (T) getApplicationContext().getBean(name, requiredType);
    }

    protected <T> T getBean(Class<T> requiredType)
    {
        Map beansOfType = getApplicationContext().getBeansOfType(requiredType);
        if(beansOfType.size() == 1)
        {
            return (T) new ArrayList(beansOfType.values()).get(0);
        }
        return null;
    }
}
