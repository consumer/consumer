package ch.mjava.consumer.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run this class for updating the schema on the database
 *
 * @author knm
 */
public class SchemaUpdate extends AbstractTest
{
    private static final Logger log = LoggerFactory
            .getLogger(SchemaUpdate.class);

    @Override
    protected String[] getConfigLocations()
    {
        return new String[]
                { "classpath:persistence-schema-update.xml" };
    }

    public void testSchemaUpdate()
    {
        log.error("Schema Update");
    }
}
