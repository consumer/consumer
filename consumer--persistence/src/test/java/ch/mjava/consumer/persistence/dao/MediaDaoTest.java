package ch.mjava.consumer.persistence.dao;

import ch.mjava.consumer.persistence.AbstractTest;
import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.persistence.beans.Media;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.List;

/**
 * Testing the user dao
 *
 * @author knm
 */
public class MediaDaoTest extends AbstractTest
{
    @Test
    public void testNoMedia() throws Exception
    {
        MediaDao mediaDao = getBean(MediaDao.BEAN_ID, MediaDao.class);
        Media media = mediaDao.get(1);
        assertNull(media);
    }

    @Test
    public void testSimpleMedia() throws Exception
    {
        MediaDao mediaDao = getBean(MediaDao.BEAN_ID, MediaDao.class);
        String title = "title";
        String number = "number";
        Media source = new Media(number, MediaType.TV_SHOW, title);
        mediaDao.save(source);

        Media media = mediaDao.get(source.getId());
        assertNotNull(media);
        assertEquals("title did not match", title, media.getTitle());
        assertEquals("number did not match", number, media.getNumber());
        assertEquals("type did not match", MediaType.TV_SHOW, media.getType());
    }

    @Test
    public void testDateTimeSaver() throws Exception
    {
        MediaDao mediaDao = getBean(MediaDao.BEAN_ID, MediaDao.class);
        String title = "bar";
        String number = "foo";
        Media source = new Media(number, MediaType.TV_SHOW, title);
        DateTime publishingDate = new DateTime();
        source.setPublishingDate(publishingDate);
        mediaDao.save(source);

        Media media = mediaDao.get(source.getId());
        assertNotNull(media);
        assertEquals("title did not match", title, media.getTitle());
        assertEquals("number did not match", number, media.getNumber());
        assertEquals("type did not match", MediaType.TV_SHOW, media.getType());
        assertEquals("publishing date did not match", publishingDate,
                media.getPublishingDate());
    }

    @Test
    public void testChildParent() throws Exception
    {
        MediaDao mediaDao = getBean(MediaDao.BEAN_ID, MediaDao.class);
        String title = "title";
        String number = "number";
        Media source = new Media(number, MediaType.TV_SHOW, title);
        Media child1 = new Media(number + "1", MediaType.TV_SHOW,
                title + "1");
        Media child2 = new Media(number + "2", MediaType.TV_SHOW,
                title + "2");
        source.addMedia(child1);
        source.addMedia(child2);
        mediaDao.save(source);
        mediaDao.save(child1);
        mediaDao.save(child2);

        Media media = mediaDao.get(source.getId());
        assertNotNull(media);

        List<? extends Media> childMedias = media.getChildMedias();
        assertNotNull(childMedias);
        assertEquals("size was wrong", 2, childMedias.size());

        List<Media> byNumber = mediaDao.findByNumber(number + "1");
        assertNotNull(byNumber);
        assertEquals(1, byNumber.size());
        Media media1 = byNumber.get(0);
        assertEquals(title + "1", media1.getTitle());
    }
}
