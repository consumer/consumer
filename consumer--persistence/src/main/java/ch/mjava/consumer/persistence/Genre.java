package ch.mjava.consumer.persistence;

/**
 * Genres.
 *
 * User: knm
 * Date: 12/10/10
 */
public enum Genre
{
    SCIFI,
    WESTERN,
    CRIME,
    THRILLER,
    FANTASY;


    @Override
    public String toString()
    {
        return this.name();
    }
}
