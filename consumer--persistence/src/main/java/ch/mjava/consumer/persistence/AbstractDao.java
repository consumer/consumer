package ch.mjava.consumer.persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract implementation of some dao stuff.
 *
 * @author knm
 */
public class AbstractDao<K extends Serializable, T extends DO<K>>
    implements DAO<K,T>
{
    private static final Logger log = LoggerFactory.getLogger(AbstractDao.class);

    private Class<T> domainClass;

    private SessionFactory sessionFactory;

    protected AbstractDao(Class<T> domainClass)
    {
        this.domainClass = domainClass;
    }

    public SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession()
    {
        return sessionFactory.getCurrentSession();
    }

    protected Class<T> getDomainClass()
    {
        return domainClass;
    }

    protected Criteria getCriteria()
    {
        return getSession().createCriteria(domainClass);
    }

    @Transactional
    public void delete(T o)
    {
        getSession().delete(o);
    }

    @Transactional
    public void save(T o)
    {
        getSession().save(o);
    }

    @Transactional
    public void update(T o)
    {
        getSession().update(o);
    }

    public T get(K id)
    {
        return (T) getSession().get(domainClass, id);
    }

    public T getNew()
    {
        try
        {
            return domainClass.newInstance();
        }
        catch(InstantiationException e)
        {
            log.error("error instantiating object", e);
        }
        catch(IllegalAccessException e)
        {
            log.error("error instantiating object", e);
        }
        return null;
    }

    public List<T> findAll(int offset, int max)
    {
        Criteria criteria = getSession().createCriteria(domainClass);
        if(offset != 0)
        {
            criteria.setFirstResult(offset);
        }

        if(max != 0)
        {
            criteria.setMaxResults(max);
        }
        return (List<T>)criteria.list();
    }

    public int countAll()
    {
        Criteria criteria = getSession().createCriteria(domainClass);
        criteria.setProjection(Projections.rowCount());
        return  (Integer)criteria.uniqueResult();
    }

}
