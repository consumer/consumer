package ch.mjava.consumer.persistence;

import java.io.Serializable;

/**
 * Media Type
 *
 * User: knm
 * Date: 12/10/10
 */
public enum MediaType implements Serializable
{
    MUSIC,
    BOOK,
    AUDIOBOOK,
    TV_SHOW,
    MOVIE
}
