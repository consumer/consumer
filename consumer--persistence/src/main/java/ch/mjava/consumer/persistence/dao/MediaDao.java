package ch.mjava.consumer.persistence.dao;

import ch.mjava.consumer.persistence.AbstractDaoList;
import ch.mjava.consumer.persistence.CriteriaFilterInterface;
import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.persistence.beans.Media;
import ch.mjava.consumer.persistence.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object
 *
 * @author knm
 */
public class MediaDao extends AbstractDao<Integer, Media>
{
    public static final String BEAN_ID = "mediaDao";

    public MediaDao()
    {
        super(Media.class);
    }



    /**
     * Find by MediaType
     *
     * @param type
     * @return
     */
    public List<Media> findByMediaType(MediaType type)
    {
        return (List<Media>) getCriteria().add(Property.forName("type").eq(type)).list();
    }

    /**
     * Find by Number and MediaType
     *
     * @param number
     * @param type
     * @return
     */
    public List<Media> findByNumberAndMediaType(String number, MediaType type)
    {
        Criteria criteria = getCriteria().add(Property.forName("number").eq(number))
                .add(Property.forName("type").eq(type));
        return (List<Media>) criteria.list();
    }

    public List<Media> findByNumber(String number)
    {
        Criteria criteria = getCriteria()
                .add(Restrictions.like("number", "%" + number + "%"));
        return (List<Media>) criteria.list();
    }

    public All getAll()
    {
        return new All(this);
    }

    public ByType getByType(MediaType type)
    {
        return new ByType(this, type);
    }


    public static class All extends AbstractDaoList<Integer, Media, Media>
    {
        String sort;
        boolean ascending;

        public All(MediaDao mediaDao)
        {
            super(mediaDao);
        }

        @Override
        protected List<Order> getOrder()
        {
            List<Order> result = new ArrayList<Order>();
            if(sort != null)
            {
                result.add(ascending ? Order.asc(sort) : Order.desc(sort));
            }
            return result;
        }

        public void setOrder(String column, boolean ascending)
        {
            this.sort = column;
            this.ascending = ascending;
        }

        @Override
        protected List<CriteriaFilterInterface> getFilter()
        {
            return new ArrayList<CriteriaFilterInterface>();
        }
    }

    public static class ByType extends All
    {
        MediaType type;
        public ByType(MediaDao mediaDao, MediaType type)
        {
            super(mediaDao);
            this.type = type;
        }

        @Override
        protected List<CriteriaFilterInterface> getFilter()
        {
            List<CriteriaFilterInterface> filters = super.getFilter();
            CriteriaFilterInterface filter = new CriteriaFilterInterface()
            {
                @Override
                public void applyFilter(Criteria criteria)
                {
                    criteria.add(Property.forName("type").eq(type));
                }
            };
            filters.add(filter);
            return filters;
        }
    }

}
