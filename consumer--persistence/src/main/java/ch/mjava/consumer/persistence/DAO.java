package ch.mjava.consumer.persistence;

import java.io.Serializable;
import java.util.List;

/**
 * Common interface for dao
 *
 * @author knm
 */
public interface DAO<K extends Serializable, T extends DO<K>>
{
    void delete(T o);

    void save(T o);

    void update(T o);

    T get(K id);

    T getNew();

    public List<T> findAll(int offset, int max);

    public int countAll();
}
