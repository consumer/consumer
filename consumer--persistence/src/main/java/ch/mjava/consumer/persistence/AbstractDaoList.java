package ch.mjava.consumer.persistence;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * abstraction for list
 *
 * @author knm
 */
public abstract class AbstractDaoList<K extends Serializable, T extends DO<K>,R>
{
    AbstractDao<K, T> dao;

    protected AbstractDaoList(AbstractDao<K, T> dao)
    {
        this.dao = dao;
    }

    public List<R> getList(Integer offset, Integer max)
    {
        List<R> result = null;
        Criteria criteria = dao.getCriteria();
        if(offset != null)
        {
            criteria.setFirstResult(offset);
        }
        if(max != null)
        {
            criteria.setMaxResults(max);
        }

        for(Order order : getOrder())
        {
            criteria.addOrder(order);
        }

        for(CriteriaFilterInterface filter : getFilter())
        {
            filter.applyFilter(criteria);
        }
        result = criteria.list();
        return result;
    }

    public int getSize()
    {
        int result = 0;
        Criteria criteria = dao.getCriteria();
        for(CriteriaFilterInterface filter : getFilter())
        {
            filter.applyFilter(criteria);
        }
        criteria.setProjection(Projections.rowCount());
        result = (Integer)criteria.uniqueResult();
        return result;
    }

    protected List<Order> getOrder()
    {
        return new ArrayList<Order>();
    }

    protected abstract List<CriteriaFilterInterface> getFilter();
}
