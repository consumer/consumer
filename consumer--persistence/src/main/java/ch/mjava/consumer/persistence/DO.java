package ch.mjava.consumer.persistence;

import java.io.Serializable;

/**
 * Common interface for all data objects (do)
 *
 */
public interface DO<K extends Serializable> extends Serializable
{
    public K getId();
}
