package ch.mjava.consumer.persistence;

import org.hibernate.Criteria;

/**
 * Filter interface
 *
 * @author knm
 */
public interface CriteriaFilterInterface
{
    public void applyFilter(Criteria criteria);
}
