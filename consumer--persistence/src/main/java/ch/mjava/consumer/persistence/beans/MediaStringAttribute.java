package ch.mjava.consumer.persistence.beans;

import ch.mjava.consumer.persistence.DO;

import javax.persistence.*;

/**
 * Simple POJO for string attributes
 *
 * @author knm
 */
@Entity(name = "MediaStringAttribute")
@Table(name = "MediaStringAttribute")
public class MediaStringAttribute implements DO<Integer>
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, unique = true, name = "name")
    private String name;

    @Column(nullable = false, unique = true, name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEDIA_ID")
    private Media media;

    public MediaStringAttribute()
    {
    }

    public MediaStringAttribute(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public Media getMedia()
    {
        return media;
    }

    public void setMedia(Media media)
    {
        this.media = media;
    }
}
