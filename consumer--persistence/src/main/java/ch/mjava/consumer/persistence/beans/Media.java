package ch.mjava.consumer.persistence.beans;

import ch.mjava.consumer.persistence.DO;
import ch.mjava.consumer.persistence.Genre;
import ch.mjava.consumer.persistence.MediaType;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

/**
 * Simple POJO for media
 *
 * @author knm
 */
@Entity(name = "Media")
@Table(name = "Medias")
public class Media implements DO<Integer>
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime publishingDate;

    @Column
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime consumedDate;

    @Column(nullable = false)
    private boolean consumed = false;

    @Column
    private String author;

    @Field
    @Column(nullable = false)
    private String title;

    @Column
    private String publisher;

    @Field("show")
    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    @CollectionOfElements
    private Set<Genre> genres = new HashSet<Genre>();

    /**
     * The parent
     */
    @ManyToOne
    private Media parent;

    /**
     * the children
     */
    @OneToMany(mappedBy = "parent")
    private List<Media> medias = new ArrayList<Media>();

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private MediaType type;

    //@Field("totalep_i")
    @Column()
    private String number;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "media")
    private List<MediaStringAttribute> attributes = new ArrayList<MediaStringAttribute>();
    public static final DateFormat FORMAT = new ISO8601CanonicalDateFormat();

    /**
     * public constructor for reflection instantiation
     */
    public Media()
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Media(String number, MediaType type, String title)
    {
        this.number = number;
        this.type = type;
        this.title = title;
    }

    public DateTime getPublishingDate()
    {
        return publishingDate;
    }

    public void setPublishingDate(DateTime publishingDate)
    {
        this.publishingDate = publishingDate;
    }

    public DateTime getConsumedDate()
    {
        return consumedDate;
    }

    public void setConsumedDate(DateTime consumedDate)
    {
        this.consumedDate = consumedDate;
    }

    public boolean isConsumed()
    {
        return consumed;
    }

    public void setConsumed(boolean consumed)
    {
        this.consumed = consumed;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getPublisher()
    {
        return publisher;
    }

    public void setPublisher(String publisher)
    {
        this.publisher = publisher;
    }

    public MediaType getType()
    {
        return type;
    }

    public void setType(MediaType type)
    {
        this.type = type;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public void clearAttributes()
    {
        attributes.clear();
    }

    public void setAttribute(String name, String value)
    {
        MediaStringAttribute attr = new MediaStringAttribute(name, value);
        MediaStringAttribute old = null;
        for(MediaStringAttribute attribute : attributes)
        {
            if(attribute.getName().equals(name))
            {
                old = attribute;
                break;
            }
        }
        attributes.remove(old);
        attributes.add(attr);
    }

    public String getAttributeValue(String name)
    {
        String result = null;
        for(MediaStringAttribute attribute : attributes)
        {
            if(attribute.getName().equals(name))
            {
                result = attribute.getValue();
                break;
            }
        }
        return result;
    }

    public List<? extends Media> getChildMedias()
    {
        return medias;
    }

    public void addMedia(Media child)
    {
        if(!medias.contains(child))
        {
            if(child instanceof Media)
            {
                Media media = (Media) child;
                child.setParent(this);
                medias.add(media);

            }

        }
    }

    public void addGenre(Genre genre)
    {
        genres.add(genre);
    }

    public Set<Genre> getGenres()
    {
        return genres;
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if(!(o instanceof Media))
        {
            return false;
        }

        Media that = (Media) o;

        if(consumed != that.consumed)
        {
            return false;
        }
        if(attributes != null ? !attributes.equals(that.attributes) : that.attributes != null)
        {
            return false;
        }
        if(author != null ? !author.equals(that.author) : that.author != null)
        {
            return false;
        }
        if(consumedDate != null ? !consumedDate.equals(that.consumedDate) : that.consumedDate != null)
        {
            return false;
        }
        if(genres != null ? !genres.equals(that.genres) : that.genres != null)
        {
            return false;
        }
        if(medias != null ? !medias.equals(that.medias) : that.medias != null)
        {
            return false;
        }
        if(!number.equals(that.number))
        {
            return false;
        }
        if(publisher != null ? !publisher.equals(that.publisher) : that.publisher != null)
        {
            return false;
        }
        if(publishingDate != null ? !publishingDate.equals(that.publishingDate) : that.publishingDate != null)
        {
            return false;
        }
        if(!title.equals(that.title))
        {
            return false;
        }
        if(type != that.type)
        {
            return false;
        }

        return true;
    }

    public Media getParent()
    {
        return parent;
    }

    public void setParent(Media media)
    {
        parent = (Media) media;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Field("airdate")
    public void setAirDate(String source)
    {
        try
        {
            Date date = FORMAT.parse(source);
            DateTime airDate = new DateTime(date);
            setPublishingDate(airDate);
        }
        catch(ParseException e)
        {
            throw new IllegalStateException("could not parse date [" +
                    source + "]", e);
        }
    }

    public String getAirDate()
    {
        String result = "";
        DateTime date = getPublishingDate();
        if(date != null)
        {
            FORMAT.format(date.toDate());
        }
        return result;
    }

     private static class ISO8601CanonicalDateFormat extends SimpleDateFormat
     {

         public static final Locale LOCALE = Locale.US;
         protected NumberFormat millisParser
      = NumberFormat.getIntegerInstance(LOCALE);

    protected NumberFormat millisFormat = new DecimalFormat(".###",
      new DecimalFormatSymbols(LOCALE));

    public ISO8601CanonicalDateFormat() {
      super("yyyy-MM-dd'T'HH:mm:ss", LOCALE);
        this.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public Date parse(String i, ParsePosition p) {
      /* delegate to SimpleDateFormat for easy stuff */
      Date d = super.parse(i, p);
      int milliIndex = p.getIndex();
      /* worry aboutthe milliseconds ourselves */
      if (null != d &&
          -1 == p.getErrorIndex() &&
          milliIndex + 1 < i.length() &&
          '.' == i.charAt(milliIndex)) {
        p.setIndex( ++milliIndex ); // NOTE: ++ to chomp '.'
        Number millis = millisParser.parse(i, p);
        if (-1 == p.getErrorIndex()) {
          int endIndex = p.getIndex();
            d = new Date(d.getTime()
                         + (long)(millis.doubleValue() *
                                  Math.pow(10, (3-endIndex+milliIndex))));
        }
      }
      return d;
    }

    public StringBuffer format(Date d, StringBuffer toAppendTo,
                               FieldPosition pos) {
      /* delegate to SimpleDateFormat for easy stuff */
      super.format(d, toAppendTo, pos);
      /* worry aboutthe milliseconds ourselves */
      long millis = d.getTime() % 1000l;
      if (0l == millis) {
        return toAppendTo;
      }
      int posBegin = toAppendTo.length();
      toAppendTo.append(millisFormat.format(millis / 1000d));
      if (DateFormat.MILLISECOND_FIELD == pos.getField()) {
        pos.setBeginIndex(posBegin);
        pos.setEndIndex(toAppendTo.length());
      }
      return toAppendTo;
    }

    public Object clone() {
      ISO8601CanonicalDateFormat c
        = (ISO8601CanonicalDateFormat) super.clone();
      c.millisParser = NumberFormat.getIntegerInstance(LOCALE);
      c.millisFormat = new DecimalFormat(".###",
        new DecimalFormatSymbols(LOCALE));
      return c;
    }
  }
}
