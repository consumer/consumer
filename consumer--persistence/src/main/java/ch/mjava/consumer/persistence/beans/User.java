package ch.mjava.consumer.persistence.beans;


import ch.mjava.consumer.persistence.DO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Very simple Pojo for user
 *
 * @author knm
 */
@Entity(name = "User")
@Table(name = "Users")
public class User implements DO<Integer>
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, unique = true, name = "email")
    private String email;

    @Column(nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "user")
    private List<UserStringAttribute> userStringAttributes = new ArrayList<UserStringAttribute>();

    @Override
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return  email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setName(String name)
    {
        this.name = name;
    }


    public String getName()
    {
        return this.name;
    }

    public List<UserStringAttribute> getUserStringAttributes()
    {
        return userStringAttributes;
    }

    public void setUserStringAttributes(String name, String value)
    {
        UserStringAttribute attr = new UserStringAttribute();
        attr.setName(name);
        attr.setValue(value);
        attr.setUser(this);
        this.userStringAttributes.add(attr);
    }
}
