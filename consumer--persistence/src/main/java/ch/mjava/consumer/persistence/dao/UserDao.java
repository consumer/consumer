package ch.mjava.consumer.persistence.dao;

import ch.mjava.consumer.persistence.AbstractDao;
import ch.mjava.consumer.persistence.beans.User;
import org.hibernate.criterion.Property;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class UserDao extends AbstractDao<Integer, User>
{
    public static final String BEAN_ID = "userDao";

    public UserDao()
    {
        super(User.class);
    }

    public User getByEmail(String email)
    {
        return (User) getCriteria().add(Property.forName("email").eq(email))
                .uniqueResult();
    }
}
