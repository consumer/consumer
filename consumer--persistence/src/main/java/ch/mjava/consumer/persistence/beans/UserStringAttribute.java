package ch.mjava.consumer.persistence.beans;

import ch.mjava.consumer.persistence.DO;

import javax.persistence.*;

/**
 * Simple POJO for string attributes
 *
 * @author knm
 */
@Entity(name = "UserStringAttribute")
@Table(name = "UserStringAttribute")
public class UserStringAttribute implements DO<Integer>
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, unique = true, name = "name")
    private String name;

    @Column(nullable = false, unique = true, name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    public UserStringAttribute()
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }
}
