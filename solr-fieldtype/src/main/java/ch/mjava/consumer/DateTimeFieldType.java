package ch.mjava.consumer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SortField;
import org.apache.solr.request.TextResponseWriter;
import org.apache.solr.request.XMLWriter;
import org.apache.solr.schema.DateField;
import org.apache.solr.schema.FieldType;
import org.apache.solr.schema.SchemaField;
import org.apache.solr.search.QParser;
import org.apache.solr.search.function.ValueSource;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;

/**
 * Own Fieldtype
 *
 */
public class DateTimeFieldType extends FieldType
{
    private DateField dateField = new DateField();

    @Override
    public Query getRangeQuery(QParser parser, SchemaField field,
            String part1, String part2, boolean minInclusive,
            boolean maxInclusive)
    {
        return dateField
                .getRangeQuery(parser, field, part1, part2, minInclusive,
                        maxInclusive);
    }

    @Override
    public void setQueryAnalyzer(Analyzer analyzer)
    {
        dateField.setQueryAnalyzer(analyzer);
    }

    @Override
    public void setAnalyzer(Analyzer analyzer)
    {
        dateField.setAnalyzer(analyzer);
    }

    @Override
    public Analyzer getQueryAnalyzer()
    {
        return dateField.getQueryAnalyzer();
    }

    @Override
    public Analyzer getAnalyzer()
    {
        return dateField.getAnalyzer();
    }

    @Override
    public String readableToIndexed(
            String val)
    {
        return dateField.readableToIndexed(val);
    }

    @Override
    public String storedToIndexed(
            Fieldable f)
    {
        return dateField.storedToIndexed(f);
    }

    @Override
    public String storedToReadable(
            Fieldable f)
    {
        return dateField.storedToReadable(f);
    }

    @Override
    public Field createField(SchemaField field, String externalVal,
            float boost)
    {
        return dateField.createField(field, externalVal, boost);
    }

    @Override
    public String toString()
    {
        return dateField.toString();
    }

    @Override
    public String getTypeName()
    {
        return dateField.getTypeName();
    }

    @Override
    public boolean multiValuedFieldCache()
    {
        return dateField.multiValuedFieldCache();
    }

    @Override
    public boolean isMultiValued()
    {
        return dateField.isMultiValued();
    }

    @Override
    public boolean isTokenized()
    {
        return dateField.isTokenized();
    }

    @Override
    public ValueSource getValueSource(SchemaField field, QParser parser)
    {
        return dateField.getValueSource(field, parser);
    }

    @Override
    public SortField getSortField(SchemaField field, boolean top)
    {
        return dateField.getSortField(field, top);
    }

    @Override
    public void write(TextResponseWriter writer, String name,
            Fieldable f) throws IOException
    {
        dateField.write(writer, name, f);
    }

    @Override
    public void write(XMLWriter xmlWriter, String name,
            Fieldable f) throws IOException
    {
        dateField.write(xmlWriter, name, f);
    }

    @Override
    public String indexedToReadable(String indexedForm)
    {
        return dateField.indexedToReadable(indexedForm);
    }

    @Override
    public DateTime toObject(Fieldable f)
    {
        Date date = dateField.toObject(f);
        return new DateTime(date);
    }

    @Override
    public String toExternal(Fieldable f)
    {
        return dateField.toExternal(f);
    }

    @Override
    public String toInternal(String val)
    {
        return dateField.toInternal(val);
    }

}
