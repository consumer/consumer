package ch.mjava.consumer.stub;

import ch.mjava.consumer.model.DefaultMediaImpl;
import ch.mjava.consumer.model.Genre;
import ch.mjava.consumer.model.Media;
import ch.mjava.consumer.model.MediaType;
import ch.mjava.consumer.model.dao.MediaDao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Stub Implementation of MediaDao
 *
 */
public class MediaDaoStubImpl implements MediaDao, Serializable
{

    @Override
    public List<Media> findByMediaType(MediaType type)
    {
        List<Media> result = new ArrayList<Media>();
        if(MediaType.TV_SHOW.equals(type))
        {
            Media show = new DefaultMediaImpl("0", type, "Babylon 5");
            result.add(show);
            show.addGenre(Genre.SCIFI);

            addSeason(show, 1);
            addSeason(show, 2);
            addSeason(show, 3);
            addSeason(show, 4);
            addSeason(show, 5);
        }
        return result;
    }

    private void addSeason(Media show, int number)
    {
        Media season = new DefaultMediaImpl("" + number, MediaType.TV_SHOW, "Season " + number);
        season.addGenre(Genre.SCIFI);
        show.addMedia(season);
        addEpisodes(season);

    }

    private void addEpisodes(Media season)
    {
        int nrOfEpisodes = 18;
        for(int i = 1; i <= 18; i++)
        {
            Media episode = new DefaultMediaImpl("S" + season.getNumber() + "E" + i, MediaType.TV_SHOW,
                    "episode title" + i);
            episode.addGenre(Genre.SCIFI);
            season.addMedia(episode);
        }
    }

    @Override
    public List<Media> findByNumberAndMediaType(String number, MediaType type)
    {
        return null;
    }

    @Override
    public List<Media> findByGenre(Genre genre)
    {
        return null;
    }

    @Override
    public List<Media> findByGenreAndMediaType(Genre genre, MediaType type)
    {
        return null;
    }
}
