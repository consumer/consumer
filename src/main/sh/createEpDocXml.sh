#!/bin/bash

SHOWNAME=$1
URL=http://localhost:8080/solr/update
htmlfile=/tmp/$$tmpfile.html
solrfile=/tmp/$$epxmlfile.xml
curl http://epguides.com/$SHOWNAME/ >$htmlfile
awk -f episoder_helper_epguides_solr.awk showid=$SHOWNAME output=$solrfile $htmlfile  >$SHOWNAME.log
SOLR_IMPORT_FILE=/tmp/import.solr.xml

echo "<add>" >  $SOLR_IMPORT_FILE
cat $solrfile >> $SOLR_IMPORT_FILE
echo "</add>" >> $SOLR_IMPORT_FILE

echo Posting file to $URL
curl $URL --data-binary @$SOLR_IMPORT_FILE -H 'Content-type:text/xml; charset=utf-8' 

#send the commit command to make sure all the changes are flushed and visible
curl $URL --data-binary '<commit/>' -H 'Content-type:text/xml; charset=utf-8'

rm -f $tempfile $SHOWNAME.log $jsonfile $htmlfile $SOLR_IMPORT_FILE


# after importing you can start a faceted search to get all shows grouped together and
# their episodes counted
# http://localhost:8080/solr/select/?wt=json&indent=on&q=*:*&fl=name&facet=true&facet.field=show

