 $(document).ready(function(){
   $("#advancedTable").hide();
   $("#toggleAdvanced").click(function(event)
   {
       var adTable = $("#advancedTable");
       if(adTable.is(':visible'))
       {
           adTable.fadeOut("slow");
       }
       else
       {
           adTable.fadeIn("slow");
       }
   });
 });