package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Example
 *
 * @author knm
 */
public class ModelChangePage extends WebPage
{
    public ModelChangePage()
    {
        final IModel<Integer> message = Model.of(0);

        add(new Label("message", message));

        add(new Link<Integer>("changeModel", message)
        {
            @Override
            public void onClick()
            {
                setModelObject(getModelObject() + 1);
            }
        });

        add( new Link<Integer>("chaneModelDirect", message)
        {
            @Override
            public void onClick()
            {
                // setting the value on the object directly will ommit
                // a call to wicket to update the pagestore and the back button
                // cannot get the last page version from the store since it
                // is not aware that the model has changed.
                getModel().setObject((getModel().getObject() + 1));
            }
        });

        add(new Link("doNothing")
        {
            @Override
            public void onClick()
            {

            }
        });
    }
}
