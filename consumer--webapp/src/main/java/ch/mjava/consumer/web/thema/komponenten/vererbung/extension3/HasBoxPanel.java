package ch.mjava.consumer.web.thema.komponenten.vererbung.extension3;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * Example
 *
 * @author knm
 */
public class HasBoxPanel extends AbstractBasePanel
{
    public HasBoxPanel(String id)
    {
        super(id);
    }

    @Override
    protected Panel getChild(String id)
    {
        return new BoxPanel(id);
    }

    @Override
    protected BasePanelLabel getMessage()
    {
        return new HasBoxLabel();
    }

    static class HasBoxLabel extends BasePanelLabel
    {
        HasBoxLabel()
        {
            super("hab ne box");
        }
    }
}
