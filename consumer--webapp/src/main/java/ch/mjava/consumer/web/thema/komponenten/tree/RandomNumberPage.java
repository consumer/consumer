package ch.mjava.consumer.web.thema.komponenten.tree;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Learning Example
 *
 * @author knm
 */
public class RandomNumberPage extends WebPage
{
    private static final List<Integer> NUMBERS = Arrays
            .asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

    public RandomNumberPage()
    {
        List<Integer> list = getNumbers();

        for(int i = 0; i < 9; i++)
        {
            add(new Label("1" + i, Model.of(list.get(i))));
        }

        add(new Link("link")
        {
            @Override
            public void onClick()
            {
                VisitLabels visitor = new VisitLabels();
                RandomNumberPage.this.visitChildren(visitor);
            }
        });

        add(new BookmarkablePageLink<RandomNumberPage>("reset",
                RandomNumberPage.class));
    }

    /**
     * randomly add numbers
     * @return
     */
    private List<Integer> getNumbers()
    {
        List<Integer> list = new ArrayList<Integer>(NUMBERS);
		List<Integer> ret = new ArrayList<Integer>();

		while (!list.isEmpty()) {
			Integer zahl = list.remove((int) (Math.random() * list.size()));
			ret.add(zahl);
		}
		return ret;
    }

    static class VisitLabels implements IVisitor<Component>
    {
        @Override
        public Object component(Component component)
        {
            if(component instanceof Label)
            {
                Object data = component.getDefaultModelObject();
                if(data instanceof Integer)
                {
                    Integer val = (Integer) data;
                    if((val % 3) == 0)
                    {
                        component.setVisible(!component.isVisible());
                    }

                }
            }
            return IVisitor.CONTINUE_TRAVERSAL;
        }
    }
}
