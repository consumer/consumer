package ch.mjava.consumer.web.thema.komponenten.vererbung.extension3;

import org.apache.wicket.markup.html.basic.Label;

/**
 * Learning example
 *
 * @author knm
 */
public class BasePanelLabel extends Label
{
    public static final String MESSAGE_ID = "message";

    public BasePanelLabel(String id)
    {
        super(MESSAGE_ID, id);
    }
}
