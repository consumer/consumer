package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
 * Learning example
 *
 * @author knm
 */
public class DummyBeanPropertyModel<T extends Serializable>
        implements IModel<T>
{
    public enum PropertySelector
    {
        NAME, AGE,
    }

    private DummyBean bean;
    private PropertySelector selector;

    public DummyBeanPropertyModel(DummyBean bean,
            PropertySelector selector)
    {
        this.bean = bean;
        this.selector = selector;
    }

    public T getObject()
    {
        switch(selector)
        {
            case NAME:
                return (T) bean.getName();
            case AGE:
                return (T) new Integer(bean.getAge());
        }
        return null;
    }

    public void setObject(T object)
    {
        switch(selector)
        {
            case NAME:
                bean.setName((String) object);
                break;
            case AGE:
                Integer age = (Integer) object;
                bean.setAge(age.intValue());
                break;
        }
    }

    public void detach()
    {
        // do nothing for now
    }

}
