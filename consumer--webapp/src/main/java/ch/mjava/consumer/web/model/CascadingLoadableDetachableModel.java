package ch.mjava.consumer.web.model;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * this Model has an inner (parent) model which gets a call to
 * {@link org.apache.wicket.model.IDetachable#detach()} when
 * {@link ch.mjava.consumer.web.model.CascadingLoadableDetachableModel#onDetach()}
 * is called
 *
 * @author knm
 */
public abstract class CascadingLoadableDetachableModel<M, P>
    extends LoadableDetachableModel<M>
{
    private IModel<? extends P> parent;

    protected CascadingLoadableDetachableModel(IModel<? extends P> parent)
    {
        super();
        this.parent = parent;
    }

    @Override
    protected void onDetach()
    {
        parent.detach();
    }

    @Override
    final protected M load()
    {
        P result = parent.getObject();
        return load(result);
    }

    protected abstract M load(P parent);
}
