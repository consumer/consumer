package ch.mjava.consumer.web.pages;

import ch.mjava.consumer.web.panels.FooterPanel;
import ch.mjava.consumer.web.panels.HeaderPanel;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.CSSPackageResource;
import org.apache.wicket.markup.html.JavascriptPackageResource;
import org.apache.wicket.markup.html.WebPage;

/**
 * Common Basepage
 *
 * @author knm
 */
public class AbstractMediaTrackerPage extends WebPage
{
    public AbstractMediaTrackerPage(PageParameters parameters)
    {
        super(parameters);
        initCommonPageProperties();
    }

    public AbstractMediaTrackerPage()
    {
        super();
        initCommonPageProperties();
    }

    private void initCommonPageProperties()
    {
        add(CSSPackageResource
                .getHeaderContribution(AbstractMediaTrackerPage.class,
                        "css/style.css"));
        add(CSSPackageResource
                .getHeaderContribution(AbstractMediaTrackerPage.class,
                        "css/media.css"));
        add(CSSPackageResource.getHeaderContribution("images"));
        add(JavascriptPackageResource
                .getHeaderContribution(AbstractMediaTrackerPage.class,
                        "jquery-1.4.4.min.js"));


        add(new HeaderPanel("header"));

        add(new FooterPanel("footer"));
    }
}
