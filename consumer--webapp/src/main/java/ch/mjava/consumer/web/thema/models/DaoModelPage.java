package ch.mjava.consumer.web.thema.models;

import ch.mjava.consumer.persistence.beans.User;
import ch.mjava.consumer.persistence.beans.UserStringAttribute;
import ch.mjava.consumer.persistence.dao.UserDao;
import ch.mjava.consumer.web.model.DaoModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

/**
 * Example
 *
 * @author knm
 */
public class DaoModelPage extends WebPage
{
    @SpringBean
    private UserDao userDao;


    public DaoModelPage()
    {
        User user = userDao.getByEmail("klaus@test.ch");

        DaoModel<Integer, User> model = new DaoModel<Integer, User>(userDao,
                user);
        List<UserStringAttribute> userStringAttributes = user
                .getUserStringAttributes();
        if(userStringAttributes != null &&
                userStringAttributes.size() < 1)
        {
            user.setUserStringAttributes("foo", "barbarossa");
            userDao.save(user);
        }

        setDefaultModel(new CompoundPropertyModel<User>(model));
        add(new Label("name"));
        add(new Label("email"));
        add(new Label("userStringAttributes[0].value"));
        add(new Link("doNothing")
        {
            @Override
            public void onClick()
            {
                // do nothing but reload
            }
        });
    }
}
