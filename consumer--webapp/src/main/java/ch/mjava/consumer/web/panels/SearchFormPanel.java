package ch.mjava.consumer.web.panels;

import ch.mjava.consumer.web.panels.form.AbstractFormPanel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class SearchFormPanel extends AbstractFormPanel
{
    private String searchString;

    public SearchFormPanel(String id)
    {
        super(id);
        Form form = new Form("searchForm");
        form.add(new TextField("searchField",
                new PropertyModel<String>(this, "searchString")));
        form.add(new Button("searchSubmit")
        {
            @Override
            public void onSubmit()
            {
                // info("searched for " + getSearchString());
            }
        });
        add(form);
    }

    public String getSearchString()
    {
        return searchString;
    }

    public void setSearchString(String searchString)
    {
        this.searchString = searchString;
    }
}
