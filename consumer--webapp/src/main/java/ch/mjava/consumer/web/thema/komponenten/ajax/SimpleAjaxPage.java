package ch.mjava.consumer.web.thema.komponenten.ajax;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 * example
 *
 * @author knm
 */
public class SimpleAjaxPage extends WebPage
{
    private Label labelRefresh, labelNoRefresh;

    public SimpleAjaxPage()
    {
        labelRefresh = new Label("refresh", "Bin gleich weg");

        labelRefresh.setOutputMarkupId(true);
        add(labelRefresh);

        labelNoRefresh = new Label("noRefresh", "bin noch da");
        add(labelNoRefresh);

        add( new AjaxLink("link")
        {
            @Override
            public void onClick(AjaxRequestTarget target)
            {
                labelRefresh.setDefaultModelObject("neuer Text");
                labelNoRefresh
                        .setDefaultModelObject("auch hier ein neuer text");
                target.addComponent(labelRefresh);
            }
        });
    }
}
