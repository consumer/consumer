package ch.mjava.consumer.web.thema.komponenten.vererbung.extension3;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * example
 *
 * @author knm
 */
public class ExtendedExtensionPage extends WebPage
{
    public ExtendedExtensionPage()
    {
        add(new HasBoxPanel("hasBox"));

        add(new AbstractBasePanel("inline")
        {
            @Override
            protected Panel getChild(String id)
            {
                return new EmptyPanel(id);
            }

            @Override
            protected BasePanelLabel getMessage()
            {
                return new BasePanelLabel("Ich hab keine Box");
            }
        });
    }
}
