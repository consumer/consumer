package ch.mjava.consumer.web.panels;

import org.apache.wicket.markup.html.form.SimpleFormComponentLabel;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Panel for the author row
 *
 * @author knm
 */
public class AuthorRowPanel extends Panel
{
    private TextField<String> authorField;

    public AuthorRowPanel(String id, IModel<?> model)
    {
        super(id, model);
        authorField = new TextField<String>("author");
        authorField.setLabel(Model.of("Author:"));
        add(new SimpleFormComponentLabel("authorLabel", authorField));
        add(authorField);
    }

    public TextField<String> getAuthorField()
    {
        return authorField;
    }
}
