package ch.mjava.consumer.web.pages;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.web.panels.RightFootPanel;
import ch.mjava.consumer.web.panels.form.MediaListFormPanel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;

/**
 * Class to Display a list of a certain type of media
 *
 * @author knm
 */
public class AbstractMediaListPage extends AbstractMediaTrackerPage
{
    protected  MediaType type = null;

    public AbstractMediaListPage(MediaType type)
    {
        this.type = type;
        Label typeLabel = new Label("typeLabel", Model.of("All"));

        if(type != null)
        {
            typeLabel = new Label("typeLabel", Model.of(type.name()));
        }

        add(typeLabel);

        add(new MediaListFormPanel("mediaListFormPanel", type));
        add(new RightFootPanel("righttfoot"));
    }


}
