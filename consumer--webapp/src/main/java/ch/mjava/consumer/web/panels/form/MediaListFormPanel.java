package ch.mjava.consumer.web.panels.form;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.persistence.beans.Media;
import ch.mjava.consumer.persistence.dao.MediaDao;
import ch.mjava.consumer.web.constangs.ParameterConstants;
import ch.mjava.consumer.web.model.DetachableMediaModel;
import ch.mjava.consumer.web.pages.EditMediaPage;
import ch.mjava.consumer.wicket.util.MediaList;
import ch.mjava.consumer.wicket.util.MediaListByType;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.util.ModelIteratorAdapter;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Iterator;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class MediaListFormPanel extends AbstractFormPanel
{
    public static final int PAGE_SIZE = 10;
    final Form<Media> form;

    @SpringBean
    MediaDao mediaDao;

    public MediaListFormPanel(String id, final MediaType type)
    {
        super(id);
        form = new Form("listForm");
        final MediaList data = new MediaListByType(mediaDao, type);

        final DataView<Media> dataView = new DataView<Media>("mediarow",data)
        {


            @Override
            protected Iterator<IModel<Media>> getItemModels()
            {
                return new ModelIteratorAdapter<Media>(data.iterator(getViewOffset(),
                        getViewOffset() + getViewSize() ))
                {
                    @Override
                    protected IModel<Media> model(Media object)
                    {
                        return new CompoundPropertyModel<Media>(
                                new DetachableMediaModel(object, mediaDao));
                    }
                };
            }

            @Override
            protected void populateItem(Item<Media> mediaItem)
            {
                IModel<Media> model = mediaItem.getModel();
                Media media = model.getObject();
                PageParameters parameters = new PageParameters();
                parameters.put(ParameterConstants.MEDIA_ID_INT, media.getId());
                BookmarkablePageLink editLink = new BookmarkablePageLink(
                        "editLink",
                        EditMediaPage.class, parameters);
                editLink.add(new Label("title"));
                mediaItem.add(editLink);
                mediaItem.add(new Label("number"));
                mediaItem.add(new Label("author"));
                mediaItem.add(new CheckBox("consumed"));
                mediaItem.add(new ActionPanel("action", model));
            }
        };

        dataView.setItemsPerPage(PAGE_SIZE);
        form.add(dataView);
        form.add(new AjaxPagingNavigator("pager",dataView));
        add(form);
    }

    private class ActionPanel extends Panel
    {
        private ActionPanel(String id, IModel<?> model)
        {
            super(id, model);

            SubmitLink submitLink = new SubmitLink("save", form)
            {
                @Override
                public void onSubmit()
                {
                    Media media = (Media) ActionPanel.this
                            .getDefaultModelObject();
                    info("clicked media " + media.getTitle());
                    mediaDao.save(media);

                }
            };
            add(submitLink);
        }
    }

}
