package ch.mjava.consumer.web.panels.form;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.persistence.beans.Media;
import ch.mjava.consumer.persistence.dao.MediaDao;
import ch.mjava.consumer.web.model.DaoModel;
import ch.mjava.consumer.web.pages.HomePage;
import ch.mjava.consumer.web.panels.AuthorRowPanel;
import org.apache.wicket.ResourceReference;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.JavascriptPackageResource;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.DateTime;
import org.odlabs.wiquery.ui.datepicker.DatePicker;

import java.util.Arrays;
import java.util.List;

/**
 * Form for creating/entering Media
 *
 * @author knm
 */
public class EditMediaForm extends AbstractFormPanel
{
    @SpringBean
    MediaDao mediaDao;

    public EditMediaForm(String id)
    {
        super(id);
        Media media = mediaDao.getNew();
        initForm(media);
    }

    /**
     * Constructor takes an id to retrieve the media.
     * @param id
     * @param mediaId
     */
    public EditMediaForm(String id, Integer mediaId)
    {
        super(id);
        Media media = mediaDao.get(mediaId);
        initForm(media);
    }

    private void initForm(Media media)
    {
        add(JavascriptPackageResource
                .getHeaderContribution(EditMediaForm.class, "custom.js"));

        DaoModel<Integer, Media> model = new DaoModel<Integer, Media>(mediaDao,
                media);
        final CompoundPropertyModel<Media> beanModel = new CompoundPropertyModel<Media>(
                model);
        final Form<Media> form = new Form<Media>("form", beanModel);
        form.setOutputMarkupId(true);

        TextField<String> titleField = new TextField<String>("title");
        titleField.setLabel(Model.of("Title:"));
        form.add(new SimpleFormComponentLabel("titleLabel", titleField));
        titleField.setRequired(true);
        form.add(titleField);

        final AuthorRowPanel authorRow = new AuthorRowPanel("authorRow",
                beanModel);
        authorRow.setVisible(false);
        form.add(authorRow);

        final TextField<String> numberField = new TextField<String>("number");
        numberField.setLabel(Model.of("Number:"));
        form.add(new SimpleFormComponentLabel("numberLabel", numberField));
        numberField.setRequired(true);
        form.add(numberField);

        IChoiceRenderer<MediaType> enumRenderer = new EnumChoiceRenderer<MediaType>(
                this);

        List<MediaType> choices = Arrays.asList(MediaType.values());
        IModel<List<? extends MediaType>> mediaTypeIModel = Model.of(choices);

        DropDownChoice<MediaType> choice = new DropDownChoice<MediaType>(
                "type", mediaTypeIModel, enumRenderer);
        choice.setOutputMarkupId(true);
        choice.add(new AjaxFormComponentUpdatingBehavior("onchange")
        {
            @Override
            protected void onUpdate(AjaxRequestTarget target)
            {
                MediaType mediaType = beanModel.getObject().getType();
                info("you chose a " + mediaType);
                target.addComponent(feedbackPanel);

                if(mediaType.equals(MediaType.BOOK))
                {
                    authorRow.setVisible(true);
                    authorRow.getAuthorField().setRequired(true);
                    numberField.setRequired(false);
                }
                else
                {
                    authorRow.setVisible(false);
                    numberField.setRequired(true);
                }

                // refresh form
                target.addComponent(form);
            }
        });

        choice.setLabel(Model.of("Type:"));
        form.add(new SimpleFormComponentLabel("typeLabel", choice));
        choice.setRequired(true);
        form.add(choice);

        CheckBox consumed = new CheckBox("consumed");
        consumed.setLabel(Model.of("Consumed:"));
        form.add(new SimpleFormComponentLabel("consumedLabel", consumed));
        form.add(consumed);

        TextArea<String> description = new TextArea<String>("description");
        description.setLabel(Model.of("Description:"));
        form.add(new SimpleFormComponentLabel("descriptionLabel", description));
        form.add(description);

//        horizontal line for splitting advanced view
        form.add(new Image("h_line.jpg",
                new ResourceReference(HomePage.class, "images/h_line.jpg")));

        DatePicker datePicker = new DatePicker<DateTime>("publishingDate");
        datePicker.setButtonImage(
                "http://jqueryui.com/demos/datepicker/images/calendar.gif");
        datePicker.setButtonImageOnly(true);
        form.add(datePicker);


        form.add(new Button("save")
        {
            @Override
            public void onSubmit()
            {
                Media mediaFromForm = form.getModel().getObject();
                info(mediaFromForm.getTitle() + " saved");
                mediaDao.save(mediaFromForm);
            }
        });
        form.add(new Button("saveAndNew")
        {
            @Override
            public void onSubmit()
            {
                Media mediaFromForm = form.getModel().getObject();
                info(mediaFromForm.getTitle() + " saved");
                mediaDao.save(mediaFromForm);
                beanModel.detach();
            }
        });
        Button deletButton = new Button("delete")
        {
            @Override
            public void onSubmit()
            {
                Media mediaFromForm = form.getModel().getObject();
                info(mediaFromForm.getTitle() + " deleted");
                mediaDao.delete(mediaFromForm);
                beanModel.detach();
                form.clearInput();
            }
        };
        deletButton.setDefaultFormProcessing(false);
        form.add(deletButton);
        add(form);
    }

}
