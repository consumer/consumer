package ch.mjava.consumer.web.thema.komponenten.vererbung.extension3;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * Learning example
 *
 * @author knm
 */
public abstract class AbstractBasePanel extends Panel
{
    protected AbstractBasePanel(String id)
    {
        super(id);

        add(getMessage());
        add(getChild("child"));
    }

    protected abstract Panel getChild(String id);

    protected abstract BasePanelLabel getMessage();


}
