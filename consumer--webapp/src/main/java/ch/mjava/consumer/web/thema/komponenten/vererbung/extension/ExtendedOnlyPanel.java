package ch.mjava.consumer.web.thema.komponenten.vererbung.extension;

/**
 * Learning example
 *
 * @author knm
 */
public class ExtendedOnlyPanel extends BasePanel
{
    public ExtendedOnlyPanel(String id)
    {
        super(id);
    }

    @Override
    protected String getMessage()
    {
        return "Extends (" +  super.getMessage() + ")";
    }
}
