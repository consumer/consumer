package ch.mjava.consumer.web.thema.models;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Learning example
 *
 * @author knm
 */
public class SubBean implements Serializable
{
    private DateTime date;

    public DateTime getDate()
    {
        return date;
    }

    public void setDate(DateTime date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "Date " + date;
    }
}
