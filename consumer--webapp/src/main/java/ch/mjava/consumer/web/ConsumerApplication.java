package ch.mjava.consumer.web;

import ch.mjava.consumer.web.converter.CustomConverterLocator;
import ch.mjava.consumer.web.pages.HomePage;
import ch.mjava.consumer.wicket.util.resource.MavenDevResourceStreamLocator;
import org.apache.wicket.IConverterLocator;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;

/**
 * Start point of the application
 *
 * @author knm
 */
public class ConsumerApplication extends WebApplication
{
    @Override
    protected void init()
    {
        super.init();

        addComponentInstantiationListener(new SpringComponentInjector(this));

        if(DEVELOPMENT.equalsIgnoreCase(getConfigurationType()))
        {
            getResourceSettings().setResourceStreamLocator(
                    new MavenDevResourceStreamLocator());
        }
    }

    @Override
    public Class<? extends Page> getHomePage()
    {
        return HomePage.class;
    }

    @Override
    protected IConverterLocator newConverterLocator()
    {
        return new CustomConverterLocator(super.newConverterLocator());
    }
}
