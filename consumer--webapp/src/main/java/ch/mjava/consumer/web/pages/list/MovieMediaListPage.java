package ch.mjava.consumer.web.pages.list;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.web.pages.AbstractMediaListPage;

/**
 * Displaying the list of movies
 *
 * @author knm
 */
public class MovieMediaListPage extends AbstractMediaListPage
{
    public MovieMediaListPage()
    {
        super(MediaType.MOVIE);
    }
}
