package ch.mjava.consumer.web.panels;

import ch.mjava.consumer.persistence.dao.MediaDao;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * FooterPanel
 *
 * @author knm
 */
public class FooterPanel extends Panel
{
    @SpringBean
    MediaDao mediaDao;


    public FooterPanel(String id)
    {
        super(id);
        add(new Image("studio.jpg", "images/studio.jpg"));

        add(new Link("generate.test.data")
        {
            @Override
            public void onClick()
            {

            }
        });
    }
}
