package ch.mjava.consumer.web.pages;

import ch.mjava.consumer.web.constangs.ParameterConstants;
import ch.mjava.consumer.web.panels.RightFootPanel;
import ch.mjava.consumer.web.panels.form.EditMediaForm;
import org.apache.wicket.PageParameters;
import org.apache.wicket.Session;

import java.util.Locale;

/**
 * Main Page to edit medias
 *
 * @author knm
 */
public class EditMediaPage extends AbstractMediaTrackerPage
{
    public EditMediaPage()
    {
        super();
        initCommonPageProperties();
        add(new EditMediaForm("editMediaPanel"));

    }

    public EditMediaPage(PageParameters parameters)
    {
        super(parameters);
        String[] values = (String[]) parameters.get(ParameterConstants.MEDIA_ID_INT);
        Integer mediaId = Integer.parseInt(values[0]);
        initCommonPageProperties();
        add(new EditMediaForm("editMediaPanel", mediaId));
    }

    private void initCommonPageProperties()
    {
        Session.get().setLocale(new Locale("de", "CH"));
        add(new RightFootPanel("righttfoot"));
    }

}
