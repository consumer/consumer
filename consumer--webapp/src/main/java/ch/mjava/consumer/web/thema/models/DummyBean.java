package ch.mjava.consumer.web.thema.models;

import java.io.Serializable;

/**
 * Learning example
 *
 * @author knm
 */
public class DummyBean implements Serializable
{
    private String name;
    private int age;
    private SubBean subBean;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public SubBean getSubBean()
    {
        return subBean;
    }

    public void setSubBean(SubBean subBean)
    {
        this.subBean = subBean;
    }

    @Override
    public String toString()
    {
        return String.format("Name: %s, age: %s, sub: %s", name, age, subBean);
    }
}
