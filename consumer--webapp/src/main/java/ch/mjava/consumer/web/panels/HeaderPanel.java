package ch.mjava.consumer.web.panels;

import ch.mjava.consumer.web.pages.EditMediaPage;
import ch.mjava.consumer.web.pages.HomePage;
import ch.mjava.consumer.web.pages.list.AllMediaListPage;
import ch.mjava.consumer.web.pages.list.BookMediaListPage;
import ch.mjava.consumer.web.pages.list.MovieMediaListPage;
import ch.mjava.consumer.web.pages.list.TVMediaListPage;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * header displaying navigation
 *
 * @author knm
 */
public class HeaderPanel extends Panel
{
    public HeaderPanel(String id)
    {
        super(id);
        add(new Image("navi_left.jpg", "images/navi_left.jpg"));
        add(new Image("navi_right.jpg", "images/navi_right.jpg"));
        add(new BookmarkablePageLink("home.page", HomePage.class));
        add(new BookmarkablePageLink("all.page", AllMediaListPage .class));
        add(new BookmarkablePageLink("tv.page", TVMediaListPage.class));
        add(new BookmarkablePageLink("movie.page", MovieMediaListPage.class));
        add(new BookmarkablePageLink("book.page", BookMediaListPage.class));
        add(new BookmarkablePageLink("edit.media.page", EditMediaPage.class));

        Link searchLink = new Link("search.link")
        {
            @Override
            public void onClick()
            {

            }
        };
        searchLink.setMarkupIdImpl("searchLink");
        searchLink.setOutputMarkupId(true);
        add(searchLink);

        add(new SearchFormPanel("searchForm"));
    }
}
