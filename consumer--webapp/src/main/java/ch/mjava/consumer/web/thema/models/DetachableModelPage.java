package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.joda.time.DateTime;

/**
 * Example
 *
 * @author knm
 */
public class DetachableModelPage extends WebPage
{
    public DetachableModelPage()
    {
        IModel<String> message = new LoadableDetachableModel<String>()
        {
            @Override
            protected String load()
            {
                return "Jetzt ist " + new DateTime();
            }
        };

        add(new Label("message", message));

        add(new Link("reload")
        {
            @Override
            public void onClick()
            {
                // do nothing. will reload the page
            }
        });
    }
}
