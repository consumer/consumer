package ch.mjava.consumer.web.pages.list;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.web.pages.AbstractMediaListPage;

/**
 * Displaying the list of movies
 *
 * @author knm
 */
public class BookMediaListPage extends AbstractMediaListPage
{
    public BookMediaListPage()
    {
        super(MediaType.BOOK);
    }
}
