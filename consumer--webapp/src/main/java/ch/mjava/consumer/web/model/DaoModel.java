package ch.mjava.consumer.web.model;

import ch.mjava.consumer.persistence.DAO;
import ch.mjava.consumer.persistence.DO;
import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
 * Model caring for data access
 *
 * @author knm
 */
public class DaoModel<K extends Serializable, T extends DO<K>>
    implements IModel<T>
{
    private DAO<K,T> dao;

    private K id;
    private transient T object;
    private transient boolean attached =false;

    public DaoModel(DAO<K, T> dao)
    {
        this.dao = dao;
    }

    public DaoModel(DAO<K, T> dao, T object)
    {
        this(dao);
        this.object = object;
        this.id = object.getId();
        attached = true;
    }

    public DaoModel(DAO<K, T> dao, K id)
    {
        this(dao);
        this.id = id;
    }

    protected T load(K id)
    {
        if(id == null)
        {
            return dao.getNew();
        }
        return dao.get(id);
    }

    public T getObject()
    {
        if(!attached)
        {
            object = load(id);
            attached = true;
        }
        return object;
    }

    public void setObject(T object)
    {
        attached = false;
        id = null;

        this.object = object;
        if(object != null)
        {
            id = this.object.getId();
            attached = true;
        }
    }

    public void detach()
    {
        object = null;
        attached = false;
    }

}
