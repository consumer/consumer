package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.joda.time.DateTime;

/**
 * Learning Example
 *
 * @author knm
 */
public class PropertyModelPage extends WebPage
{
    public PropertyModelPage()
    {
        DummyBean bean = new DummyBean();

        PropertyModel<String> nameModel = new PropertyModel<String>(bean,
                "name");
        PropertyModel ageModel = new PropertyModel<Integer>(bean, "age");
        PropertyModel dateModel = new PropertyModel<DateTime>(bean,
                "subBean.date");

        nameModel.setObject("Klaus");
        ageModel.setObject(28);
        dateModel.setObject(new DateTime());

        add(new Label("name", nameModel));
        add(new Label("age", ageModel));
        add(new Label("date", dateModel));

        add(new Label("toString", bean.toString()));
    }
}
