package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.joda.time.DateTime;

/**
 * Bad Examplde
 *
 * @author knm
 */
public class DetachedDetachedModelPage extends WebPage
{
    public DetachedDetachedModelPage()
    {
        final IModel<DateTime> dateModel = new LoadableDetachableModel<DateTime>()
        {
            @Override
            protected DateTime load()
            {
                return new DateTime();
            }
        };

        IModel<String> messsage = new LoadableDetachableModel<String>()
        {
            @Override
            protected String load()
            {
                return "Jetzt ist " + dateModel.getObject();
            }
        };

        add(new Label("message", messsage));

        add(new Link("doNothing")
        {
            @Override
            public void onClick()
            {
                // do nothing
            }
        });

        add(new Link("detach")
        {
            @Override
            public void onClick()
            {
                dateModel.detach();
            }
        });


    }
}
