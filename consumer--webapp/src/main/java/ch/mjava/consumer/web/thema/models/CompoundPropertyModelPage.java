package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;
import org.joda.time.DateTime;

/**
 * Learning example
 *
 * @author knm
 */
public class CompoundPropertyModelPage extends WebPage
{
    public CompoundPropertyModelPage()
    {
        DummyBean bean = new DummyBean();
        bean.setSubBean(new SubBean());
        bean.getSubBean().setDate(new DateTime());
        bean.setName("name");
        bean.setAge(42);


        CompoundPropertyModel<DummyBean> model
                = new CompoundPropertyModel<DummyBean>(bean);
        setDefaultModel(model);

        add(new Label("name"));
        add(new Label("age"));
        add(new Label("subBean.date"));
        add(new Label("toString", bean.toString()));
    }
}
