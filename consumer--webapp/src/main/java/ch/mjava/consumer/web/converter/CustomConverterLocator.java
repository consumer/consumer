package ch.mjava.consumer.web.converter;

import ch.mjava.consumer.wicket.convert.DateTimeConverter;
import org.apache.wicket.IConverterLocator;
import org.apache.wicket.util.convert.IConverter;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class CustomConverterLocator implements IConverterLocator
{
    private IConverterLocator fallback;

    private Map<Class<?>, IConverter> customMap = new HashMap<Class<?>, IConverter>();

    {
        customMap.put(DateTime.class, new DateTimeConverter(false));
    }

    public CustomConverterLocator(IConverterLocator fallback)
    {
        this.fallback = fallback;
    }

    @Override
    public IConverter getConverter(Class<?> type)
    {
        IConverter ret = customMap.get(type);

        if(ret == null)
        {
            ret = fallback.getConverter(type);
        }
        return ret;
    }
}
