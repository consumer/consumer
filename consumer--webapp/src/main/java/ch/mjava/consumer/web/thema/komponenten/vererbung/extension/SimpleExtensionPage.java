package ch.mjava.consumer.web.thema.komponenten.vererbung.extension;

import org.apache.wicket.markup.html.WebPage;

/**
 * Learning example
 *
 * @author knm
 */
public class SimpleExtensionPage extends WebPage
{
    public SimpleExtensionPage()
    {
        add(new BasePanel("base"));
        add(new ExtendedOnlyPanel("extended"));
        add(new OverrideWithMarkupPanel("override"));
        add(new ExtendedWithMarkupPanel("withmarkup"));
    }
}

