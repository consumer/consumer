package ch.mjava.consumer.web.model;

import ch.mjava.consumer.persistence.beans.Media;
import ch.mjava.consumer.persistence.dao.MediaDao;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class DetachableMediaModel extends LoadableDetachableModel<Media>
{
    @SpringBean
    MediaDao mediaDao;

    private final int id;

    public DetachableMediaModel(Media object, MediaDao dao)
    {
        this(object.getId());
        this.mediaDao = dao;
    }

    public DetachableMediaModel(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this)
        {
            return true;
        }
        else if(obj == null)
        {
            return false;
        }
        else if(obj instanceof DetachableMediaModel)
        {
            DetachableMediaModel other = (DetachableMediaModel) obj;
            return other.id == id;
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return Long.valueOf(id).hashCode();
    }

    @Override
    protected Media load()
    {
        return mediaDao.get(id);
    }
}
