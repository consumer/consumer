package ch.mjava.consumer.web.pages.list;

import ch.mjava.consumer.web.pages.AbstractMediaListPage;

/**
 * Extending abstract media list for a list of all mediatypes
 *
 * @author knm
 */
public class AllMediaListPage extends AbstractMediaListPage
{
    public AllMediaListPage()
    {
        super(null);
    }
}
