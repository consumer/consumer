package ch.mjava.consumer.web.pages;

import ch.mjava.consumer.web.panels.RightFootPanel;
import org.apache.wicket.markup.html.image.Image;

/**
 * Starting point
 *
 * @author knm
 */
public class HomePage extends AbstractMediaTrackerPage
{
    public HomePage()
    {
        add(new Image("h_line.jpg", "images/h_line.jpg"));
        add(new RightFootPanel("righttfoot"));
    }
}
