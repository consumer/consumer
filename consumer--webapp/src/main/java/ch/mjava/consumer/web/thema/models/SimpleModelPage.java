package ch.mjava.consumer.web.thema.models;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.joda.time.DateTime;

/**
 * Example
 *
 * @author knm
 */
public class SimpleModelPage extends WebPage
{
    public SimpleModelPage()
    {
        IModel<String> message = Model.of("Initialwert");
        message.setObject("Jetzt ist " + new DateTime());
        add(new Label("message", message));
    }
}
