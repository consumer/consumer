package ch.mjava.consumer.web.thema.komponenten.vererbung.extension;

import org.apache.wicket.markup.html.basic.Label;

/**
 * Learning example
 *
 * @author knm
 */
public class ExtendedWithMarkupPanel extends BasePanel
{
    public ExtendedWithMarkupPanel(String id)
    {
        super(id);
        add(new Label("message2", "Extendend with markup"));
    }
}
