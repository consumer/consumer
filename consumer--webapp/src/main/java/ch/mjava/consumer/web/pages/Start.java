package ch.mjava.consumer.web.pages;

import ch.mjava.consumer.persistence.beans.User;
import ch.mjava.consumer.persistence.dao.UserDao;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

/**
 * Home page
 *
 * @author knm
 */
public class Start extends WebPage
{
    @SpringBean(name = UserDao.BEAN_ID)
    private UserDao userDao;

    public Start()
    {
        LoadableDetachableModel<List<User>> userModel =
                new LoadableDetachableModel<List<User>>()
                {
                    @Override
                    protected List<User> load()
                    {
                        return userDao.findAll(0, 10);
                    }
                };

        ListView<User> userList = new ListView<User>("userList", userModel)
        {
            @Override
            protected void populateItem(ListItem<User> item)
            {
                item.add(new Label("name", item.getModelObject().getName()));
            }
        };
        add(userList);
    }
}
