package ch.mjava.consumer.web.panels;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * Sidebar panel
 *
 * @author knm
 */
public class SideBarPanel extends Panel
{
    public SideBarPanel(String id)
    {
        super(id);
    }
}
