package ch.mjava.consumer.web.pages.list;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.web.pages.AbstractMediaListPage;

/**
 * Extending abstract media list for mediatype tv
 *
 * @author knm
 */
public class TVMediaListPage extends AbstractMediaListPage
{
    public TVMediaListPage()
    {
        super(MediaType.TV_SHOW);
    }
}
