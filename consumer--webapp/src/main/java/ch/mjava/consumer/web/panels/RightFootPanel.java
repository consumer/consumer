package ch.mjava.consumer.web.panels;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Right footer panel
 *
 * @author knm
 */
public class RightFootPanel extends Panel
{
    public RightFootPanel(String id)
    {
        super(id);
        add(new Image("home.jpg", "images/home_icon.jpg"));
        add(new Image("light.jpg", "images/light.jpg"));
        add(new Image("rpt.jpg", "images/rpt.jpg"));
    }
}
