package ch.mjava.consumer.web.thema.komponenten.vererbung.extension;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Learning example
 *
 * @author knm
 */
public class BasePanel extends Panel
{
    public BasePanel(String id)
    {
        super(id);
        add(new Label("message", getMessage()));
    }

    protected String getMessage()
    {
        return "Base";
    }
}
