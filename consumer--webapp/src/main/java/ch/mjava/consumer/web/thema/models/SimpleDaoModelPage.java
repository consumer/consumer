package ch.mjava.consumer.web.thema.models;

import ch.mjava.consumer.persistence.beans.User;
import ch.mjava.consumer.persistence.dao.UserDao;
import ch.mjava.consumer.web.model.CascadingLoadableDetachableModel;
import ch.mjava.consumer.web.model.DaoModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * Example
 *
 * @author knm
 */
public class SimpleDaoModelPage extends WebPage
{
    @SpringBean
    private UserDao userDao;


    public SimpleDaoModelPage()
    {
        User user = userDao.getByEmail("klaus@test.ch");

        DaoModel<Integer, User> model = new DaoModel<Integer, User>(userDao,
                user != null ? user.getId() : null);

        IModel<String> nameModel = new CascadingLoadableDetachableModel<String, User>(
                model)
        {

            @Override
            protected String load(User parent)
            {
                String result = null;
                if(parent != null)
                {
                    result = parent.getName();
                }
                return result;
            }
        };

        add(new Label("name", nameModel));
        add(new Link("doNothing")
        {
            @Override
            public void onClick()
            {
                // do nothing but reload
            }
        });
    }
}
