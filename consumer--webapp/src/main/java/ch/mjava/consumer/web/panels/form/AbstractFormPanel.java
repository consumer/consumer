package ch.mjava.consumer.web.panels.form;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Abstract Form Page, providing a feedback panel
 *
 * @author knm
 */
public abstract class AbstractFormPanel extends Panel
{
    protected FeedbackPanel feedbackPanel;

    public AbstractFormPanel(String id)
    {
        super(id);
        feedbackPanel = new FeedbackPanel("feedback");
        feedbackPanel.setOutputMarkupId(true);
        add(feedbackPanel);
    }

    protected FeedbackPanel getFeedbackPanel()
    {
        return feedbackPanel;
    }

    protected void updateFeedbackPanel(AjaxRequestTarget target)
    {
        target.addComponent(feedbackPanel);
    }
}
