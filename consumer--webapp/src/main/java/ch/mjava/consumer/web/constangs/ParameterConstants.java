package ch.mjava.consumer.web.constangs;

/**
 * Constants for page parameters
 *
 * @author knm
 */
public interface ParameterConstants
{
    public static final String MEDIA_ID_INT = "media-id";
}
