package ch.mjava.consumer.web.thema.komponenten.vererbung.extension3;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * Learning example
 *
 * @author knm
 */
public class BoxPanel extends Panel
{
    public BoxPanel(String id)
    {
        super(id);
    }
}
