package ch.mjava.consumer.wicket.util;

import ch.mjava.consumer.persistence.beans.Media;
import ch.mjava.consumer.persistence.dao.MediaDao;
import ch.mjava.consumer.web.model.DaoModel;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortState;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.extensions.markup.html.repeater.util.SingleSortState;
import org.apache.wicket.model.IModel;

import java.util.Iterator;

/**
 * List for medias
 *
 * @author knm
 */
public class MediaList implements ISortableDataProvider<Media>
{
    MediaDao mediaDao;

    transient MediaDao.All all;
    boolean attached = false;

    ISortState sortState = new SingleSortState();

    public MediaList(MediaDao mediaDao)
    {
        this.mediaDao = mediaDao;
    }

    protected MediaDao.All load()
    {
        if(!attached)
        {
            all = mediaDao.getAll();
            if(sortState != null)
            {
                all.setOrder(null, true);
                String[] properties = {"title", "number"};
                for(String property : properties)
                {
                    int sortOrder = sortState
                            .getPropertySortOrder(property);
                    switch(sortOrder)
                    {
                        case ISortState.ASCENDING:
                            all.setOrder(property, true);
                            break;
                        case ISortState.DESCENDING:
                            all.setOrder(property, true);
                            break;
                    }
                }
            }
        }
        attached = true;
        return all;
    }

    @Override
    public Iterator<Media> iterator(int first, int count)
    {
        return load().getList(first, count).iterator();
    }

    @Override
    public IModel<Media> model(Media object)
    {
        return new DaoModel<Integer, Media>(mediaDao, object);
    }

    @Override
    public int size()
    {
        return load().getSize();
    }

    @Override
    public void detach()
    {
        all = null;
        attached = false;
    }

    @Override
    public ISortState getSortState()
    {
        return sortState;
    }

    @Override
    public void setSortState(ISortState state)
    {
        sortState = state;
        detach();
    }
}
