package ch.mjava.consumer.wicket.util;

import ch.mjava.consumer.persistence.MediaType;
import ch.mjava.consumer.persistence.dao.MediaDao;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortState;

/**
 * TODO: Doku
 *
 * @author knm
 */
public class MediaListByType extends MediaList
{
    MediaType type;

    public MediaListByType(MediaDao mediaDao, MediaType type)
    {
        super(mediaDao);
        this.type = type;
    }

    @Override
    protected MediaDao.All load()
    {
         if(!attached)
        {
            if(type != null)
            {
                all = mediaDao.getByType(type);
            }
            else
            {
                all = mediaDao.getAll();
            }
            if(sortState != null)
            {
                all.setOrder(null, true);
                String[] properties = {"title", "number"};
                for(String property : properties)
                {
                    int sortOrder = sortState
                            .getPropertySortOrder(property);
                    switch(sortOrder)
                    {
                        case ISortState.ASCENDING:
                            all.setOrder(property, true);
                            break;
                        case ISortState.DESCENDING:
                            all.setOrder(property, true);
                            break;
                    }
                }
            }
        }
        attached = true;
        return all;
    }
}
