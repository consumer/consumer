package ch.mjava.consumer.wicket.convert;

import org.apache.wicket.datetime.StyleDateConverter;
import org.apache.wicket.util.convert.IConverter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Locale;

/**
 * Converter for joda DateTime. Uses the date converter for the real conversion.
 *
 * @author knm
 */
public class DateTimeConverter implements IConverter
{
    private StyleDateConverter innerConverter;

    public DateTimeConverter(String dateStyle, boolean applyTimeZoneDifference)
    {
        innerConverter = new StyleDateConverter(dateStyle, applyTimeZoneDifference);
    }

    public DateTimeConverter(boolean applyTimeZoneDifference)
    {
        innerConverter = new StyleDateConverter(applyTimeZoneDifference);
    }

    @Override
    public DateTime convertToObject(String value, Locale locale)
    {
        Date date = innerConverter.convertToObject(value, locale);
        return new DateTime(date);
    }

    @Override
    public String convertToString(Object value, Locale locale)
    {
        DateTime dateTime = (DateTime) value;
        Date date = dateTime.toDate();
        return innerConverter.convertToString(date, locale);
    }
}
