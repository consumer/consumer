package ch.mjava.consumer.wicket.util.resource;


import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.resource.locator.ResourceStreamLocator;

import java.io.File;

/**
 * Enabling of loading of markup files from src/main instead of target
 *
 * @author knm
 */
public class MavenDevResourceStreamLocator extends ResourceStreamLocator
{
    public static final String PREFIX = "src/main/resources/";

    @Override
    public IResourceStream locate(Class<?> clazz, String path)
    {
        IResourceStream located = getFileSysResourceStream(path);
        if(located != null)
        {
            return located;
        }
        return super.locate(clazz, path);
    }

    private IResourceStream getFileSysResourceStream(String path)
    {
        IResourceStream result = null;
        File file = new File(PREFIX + path);
        if(file.exists() && file.isFile())
        {
            result = new FileResourceStream(file);
        }
        return result;
    }
}
