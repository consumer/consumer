#!/bin/sh
MAVEN_OPTS="-Xmx256m -Dwicket.configuration=development"
MAVEN_OPTS="${MAVEN_OPTS} -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8787"
export MAVEN_OPTS
mvn jetty:run
